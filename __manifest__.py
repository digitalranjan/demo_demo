# -*- coding: utf-8 -*-

{
    'name': 'Demo Modull',
    'version': '16.0',
    'summary': 'Demo',
    'sequence': 10,
    'description': """Account Automatic Transfer""",
    'category': 'Accounting/Accounting',
    'website': 'https://www.oddd.com',
    'depends': ['base','purchase'],
    'data': [
        'security/ir.model.access.csv',
        'views/demo1_views.xml',
        'views/demo2_views.xml',
        'views/account_move_views.xml',
        'views/purchase_order_views.xml',
        'views/purchase_order_views.xml',
        'views/res_partner_views.xml',
        'views/sale_order_lines_views.xml',
        'views/sale_order_views.xml',
        'views/menu.xml',
    ],
    'demo': [],
    'installable': True,
    'auto_install': False,
    'license': 'LGPL-3',
}
