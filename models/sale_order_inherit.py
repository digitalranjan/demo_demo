# -*- coding: utf-8 -*-

from odoo import models, fields, api


class SaleOrderline(models.Model):
    """ Sale order Line Field Inherit"""
    _inherit = "sale.order.line"

    field_id = fields.Many2one('comodel.name', string='Filed Name')

    # sale order line to my_style to product onchange Function
    @api.onchange('product_id')
    def _onchange_product_style(self):
        if self.product_id.product_style:
            self.product_style_id = self.product_id.product_style
