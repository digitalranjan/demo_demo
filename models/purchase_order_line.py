# -*- coding: utf-8 -*-

from odoo import fields,  models

class PurchaseOrderLine(models.Model):
    """Purchase Order form, customer field"""
    _inherit = 'purchase.order.line'

    is_required = fields.Boolean(string="Is Required")

    @api.model_create_multi
    def create(self, vals_list):
        res = super(SaleOrder, self).create(vals_list)
        if res.tag_ids:
            message = 'Record: %s' % ', '.join(res.tag_ids.mapped('name'))
            res.message_post(body=message)
        return res

    def write(self, vals):
        res = super(SaleOrder, self).write(vals)
        if self.tag_ids:
            list = []
            for rec in self.tag_ids:
                list.append(rec.name)
            self.message_post(body=_(
                "Updated Tags Are :   %s.", ','.join(map(str, list))))
        return res