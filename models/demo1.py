# -*- coding: utf-8 -*-

from odoo import api, fields, models, _


class DemoCLass(models.Model):
    _name = 'demo.name'
    _description = "demo details"

    #fields
    nami = fields.Char(string="Nami", required=True)
    # Selection

    #string required help index default readonly translate groups
    # String
    """ The common attribute used for every field is String."""
    name = fields.Char(string="Field name")

    # Required
    """ We can add required=True, which will make them as mandatory field """
    name = fields.Char(string="Field name", required=True)

    # Help
    """ Provides help tooltips for users in the UI. """
    name = fields.Char(string="Name", help="Enter the description")

    # Index
    """ Requests that Odoo creates a Database Index on the column. """
    name = fields.Char(string="Record name", index=True)

    # Default
    """ We can assign default values for the fields by using the default attributes. """
    name = fields.Char(string="Field name", default='John')

    # Readonly
    """ If we make a field as a read-only field, then we cannot change the field value manually. """
    name = fields.Char(string="Name", readonly=True)

    # Translate
    """ When the translate attribute is set to true, it makes the field translatable """
    name = fields.Char(string="Name", translate=True)

    # Groups
    """It is used to make the field only available for some user groups."""
    name = fields.Char(string="Name", groups="base.user_group")

    # Selection
    """"""
    aselection = fields.Selection([('a', 'A')])
    aselection = fields.Selection(selection=[('a', 'A')])

    # Many2one
    """Store a relation against a co-model:"""
    arel_id = fields.Many2one('res.users')
    arel_id = fields.Many2one(comodel_name='res.users')
    an_other_rel_id = fields.Many2one(comodel_name='res.partner', delegate=True)

    # One2many
    """ Store a relation against many rows of co-model: """
    arel_ids = fields.One2many('res.users', 'rel_id')
    arel_ids = fields.One2many(comodel_name='res.users', inverse_name='rel_id')

    # Many2many
    """Store a relation against many2many rows of co-model:"""
    arel_ids = fields.Many2many('res.users')
    arel_ids = fields.Many2many(comodel_name='res.users',
                                relation='table_name',
                                column1='col_name',
                                column2='other_col_name')

    # Computed Fields
    class AModel(models.Model):
        _name = 'a_name'

        computed_total = fields.Float(compute='compute_total')

        def compute_total(self):
            ...
            self.computed_total = 5 + 5
