# -*- coding: utf-8 -*-

from odoo import fields,  models

class PurchaseOrder(models.Model):
    """Purchase Order form, customer field"""
    _inherit = 'purchase.order'

    is_required = fields.Boolean(string="Is Required")