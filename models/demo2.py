# -*- coding: utf-8 -*-

from odoo import models, fields


class MyModule(models.Model):
    _name = 'my.module'

    name = fields.Char(string='Name', required=True)
    description = fields.Text(string='Description')


class SomeModel(models.Model):
    _inherits = 'some.model'

    name = fields.Char(string='Name', required=True)
