# -*- coding: utf-8 -*-

from odoo import fields, models, api, _
from odoo.exceptions import ValidationError


class Partner(models.Model):
    """Customer Master inherit"""
    _inherit = 'res.partner'

    is_filter = fields.Boolean(string='Product pricelist')
    priority = fields.Selection([('0', 'No Priority'),
                                 ('1', 'Low Priority'),
                                 ('2', 'Nice To Have'),
                                 ('3', 'Important'),
                                 ('4', 'High'),
                                 ('5', 'Very High')], string="Customer Rating", default="2")

    @api.onchange('phone')
    def _check_mobile_number(self):
        for record in self:
            res = record.phone
            if record.phone and len(self.phone) != 10:
                raise ValidationError(_("Please enter only 10 digits of your phone number!!!"))
            elif record.phone and len(self.phone) == 10:
                hyp = res[:3] + '-' + res[3:6] + '-' + res[6:]
                print(hyp)
                record.phone = hyp
