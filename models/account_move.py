# -*- coding: utf-8 -*-

from odoo import fields, models, api


class AccountInvoice(models.Model):
    _inherit = 'account.move'

    no_of_lines = fields.Integer(compute='_compute_no_of_lines', string=' Total No Products')

    @api.depends('invoice_line_ids')
    def _compute_no_of_lines(self):
        for line in self:
            line.no_of_lines = len(line.invoice_line_ids)
