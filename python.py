from datetime import datetime


def _compute_year(self):
    print(self.year_entry)
    current_year = datetime.datetime.today().year
    star_year = 2010
    academic_years = []
    for year in range(star_year, current_year + 1):
        academic_year = f"{year}-{year + 1}"
        academic_years.append(academic_year)
    return academic_years